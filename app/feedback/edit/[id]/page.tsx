'use client'
import Image from 'next/image'

import { ProductRequest } from '@/types/productRequest'
import { useDataContext } from '@/context/datacontext'

import BackButton from '@/components/navigation/backbutton'
import EditFeedbackForm from '@/components/form/edit'

import EditIcon from '@/assets/shared/icon-edit-feedback.svg'
import styles from './page.module.scss'

function filterRequestWithId(id: number, productRequests: ProductRequest[]) {
  return productRequests.filter((requests) => requests.id == id)
}

export default function EditFeedback({ params }: { params: { id: number } }) {
  const { productRequests } = useDataContext()

  const req = filterRequestWithId(params.id, productRequests)[0]

  return (
    <>
      {req && (
        <div>
          <BackButton />

          <div className={styles['edit-cont']}>
            <Image
              src={EditIcon}
              alt='Edit'
              className={styles['edit-cont__img']}
            />
            <div className={styles['edit-cont__heading']}>
              Editing {`'${req.title}'`}
            </div>

            <EditFeedbackForm req={req} />
          </div>
        </div>
      )}
    </>
  )
}
