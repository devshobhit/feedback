'use client'
import { ChangeEvent, useState } from 'react'
import { useRouter } from 'next/navigation'

import { useDataContext } from '@/context/datacontext'

import BackButton from '@/components/navigation/backbutton'
import Select from '@/components/select'

import styles from './page.module.scss'

const categories: string[] = ['feature', 'ui', 'ux', 'enhancement', 'bug']

// TODO: Add Error Boundary in the code
export default function AddFeedback() {
  const router = useRouter()

  const [title, setTitle] = useState('')
  const [category, setCategory] = useState('feature')
  const [description, setDescription] = useState('')
  const { addFeedback } = useDataContext()

  function changecategory(
    // option: 'feature' | 'ui' | 'ux' | 'enhancement' | 'bug'
    option: string
  ) {
    setCategory(option)
  }

  function handleTitleChange(e: ChangeEvent<HTMLInputElement>) {
    setTitle(e.target.value)
  }
  function handleDescChange(e: ChangeEvent<HTMLInputElement>) {
    setDescription(e.target.value)
  }

  function handleSave() {
    addFeedback &&
      addFeedback({
        title: title,
        category: category,
        description: description,
      })
    router.back()
  }

  return (
    <div className={styles['cont-wrapper']}>
      <div className={styles['cont']}>
        <div className={styles['cont-nav']}>
          <BackButton />
        </div>

        <div className={styles['cont-form']}>
          <div className={styles['cont-header']}>Create New Feedback</div>

          <div className={styles['cont-title']}>Feedback Title</div>
          <div className={styles['cont-subtitle']}>
            Add a short, descriptive headline
          </div>
          <input
            type='text'
            value={title}
            onChange={handleTitleChange}
            className={styles['cont-input']}
          />

          <div className={styles['cont-title']}>Category</div>
          <div className={styles['cont-subtitle']}>
            Choose a category for your feedback
          </div>
          <Select
            options={categories}
            active={category}
            changeActive={changecategory}
          />

          <div className={styles['cont-title']}>Feedback Detail</div>
          <div className={styles['cont-subtitle']}>
            Include any specific comments on what should be improved,added.etc.
          </div>
          <input
            type='text'
            value={description}
            onChange={handleDescChange}
            className={styles['cont-input']}
          />

          <div>
            <button
              className='btn btn--secondary btn--wide'
              onClick={() => router.back()}
            >
              Cancel
            </button>
            <button className='btn btn--primary btn--wide' onClick={handleSave}>
              Save Changes
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
