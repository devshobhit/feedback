'use client'
import Link from 'next/link'

import { ProductRequest } from '@/types/productRequest'
import { useDataContext } from '@/context/datacontext'

import ProductRequestCont from '@/components/containers/productReq'
import AddComment from '@/components/comment/addcomment'
import CommentsCont from '@/components/containers/comments'
import BackButton from '@/components/navigation/backbutton'

import styles from './page.module.scss'

// TODO: Error Handling

function filterRequestWithId(id: number, productRequests: ProductRequest[]) {
  return productRequests.filter((requests) => requests.id == id)
}

export default function Page({ params }: { params: { id: number } }) {
  const { productRequests, currentUser } = useDataContext()

  const req = filterRequestWithId(params.id, productRequests)

  return (
    <>
      <div className={styles['cont']}>
        <div className={styles['topbar']}>
          <BackButton />

          <Link href={`/feedback/edit/${params.id}`}>
            <button className={'btn btn--wide'}>Edit Feedback</button>
          </Link>
        </div>
        <ProductRequestCont req={req[0]} />
        <CommentsCont id={params.id} comments={req[0]?.comments} />
        {/* <Comment comments={req[0].comments} /> */}
        <AddComment id={params.id} user={currentUser} />
      </div>
    </>
  )
}
