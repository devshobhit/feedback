import './global.scss'
import { Inter } from 'next/font/google'
import { DataProvider } from '@/context/datacontext'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Feedback Application',
  description: 'Feedback Application',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang='en'>
      <DataProvider>
        <body className={inter.className}>{children}</body>
      </DataProvider>
    </html>
  )
}
