import Link from 'next/link'

import DataGrid from '@/components/roadmap/datagrid'
import BackButton from '@/components/navigation/backbutton'

import styles from './page.module.scss'

function Header() {
  return (
    <div className={styles['header']}>
      <div className={styles['header__left']}>
        <div>
          <BackButton />
        </div>
        <div className={styles['header__title']}>Roadmap</div>
      </div>
      <div className={styles['header__right']}>
        <Link href={'/feedback/add'}>
          <button className='btn btn--wide btn--primary'>+ Add Feedback</button>
        </Link>
      </div>
    </div>
  )
}

function Roadmap() {
  return (
    <>
      <div className={styles['cont']}>
        <Header />
        <DataGrid />
      </div>
    </>
  )
}

export default Roadmap
