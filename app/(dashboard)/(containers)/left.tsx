import { BrandCard, RoadmapCard, TagCard } from '@/components/card'

import styles from '../page.module.scss'

export default function LeftCont({
  planned,
  live,
  inprogress,
  activeFilter,
  filters,
  changeActiveFilter,
  openMenu,
}: {
  planned: number
  live: number
  inprogress: number
  activeFilter: string
  filters: string[]
  changeActiveFilter: (arg: string) => void
  openMenu: () => void
}) {
  return (
    <>
      <div className={styles['cont__left']}>
        <BrandCard openMenu={openMenu} />
        <TagCard
          activeFilter={activeFilter}
          changeActiveFilter={changeActiveFilter}
          filters={filters}
        />
        <RoadmapCard planned={planned} inprogress={inprogress} live={live} />
      </div>
    </>
  )
}
