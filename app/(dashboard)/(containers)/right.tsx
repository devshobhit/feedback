'use client'
import { useState, useEffect } from 'react'

import { ProductRequest } from '@/types/productRequest'
import { sortTypes } from '@/types/sortTypes'

import Banner from '../banner'

import {
  ScrollableCont,
  ProductReqCont,
  EmptyCont,
} from '@/components/containers'

import { sortProductRequests } from '@/utils/sort'
import styles from '../page.module.scss'

export default function RightCont({
  productRequests,
}: {
  productRequests: ProductRequest[]
}) {
  const sortFilters = [
    'Most Upvotes',
    'Most Comments',
    'Least Upvotes',
    'Least Comments',
  ]

  const [activeSortFilter, setActiveSortFilter] =
    useState<sortTypes>('Most Upvotes')

  // FIXME: Look for better ways to mange the sortedRequests state
  const [sortedRequests, setSortedRequests] = useState<ProductRequest[]>([])

  // FIXME: Temporary workaround
  useEffect(() => {
    setSortedRequests(productRequests)
  }, [productRequests])

  useEffect(() => {
    const updatedRequests = sortProductRequests(
      productRequests,
      activeSortFilter
    )
    setSortedRequests(updatedRequests)
  }, [activeSortFilter, productRequests])

  return (
    <>
      <div className={styles['cont__right']}>
        <Banner
          suggestions={productRequests.length}
          filters={sortFilters}
          setActiveSortFilter={setActiveSortFilter}
          activeSortFilter={activeSortFilter}
        />
        <div className={styles['product-requests']}>
          {sortedRequests.length > 0 ? (
            <>
              <ScrollableCont>
                {sortedRequests.map((request) => {
                  return (
                    <div key={'Poduct Request-' + request.id}>
                      <ProductReqCont req={request} />
                    </div>
                  )
                })}
              </ScrollableCont>
            </>
          ) : (
            <EmptyCont />
          )}
        </div>
      </div>
    </>
  )
}
