'use client'
import { useEffect, useState } from 'react'

import { ProductRequest } from '@/types/productRequest'
import { useDataContext } from '@/context/datacontext'

import { BrandCard, RoadmapCard, TagCard } from '@/components/card'
import MobileNav from '@/components/navigation/mobileNav'
import LeftCont from './(containers)/left'
import RightCont from './(containers)/right'

import styles from './page.module.scss'

// TODO: Add boundary to check if the active product requests are empty display suspense component

function roadmapCardData(productRequests: ProductRequest[]) {
  const totalPlanned = productRequests
    ? productRequests.filter(
        (request) => request.status.toLowerCase() === 'planned'
      ).length
    : 0
  const totalLive = productRequests
    ? productRequests.filter(
        (request) => request.status.toLowerCase() === 'live'
      ).length
    : 0
  const totalInProgress = productRequests
    ? productRequests.filter(
        (request) => request.status.toLowerCase() === 'in-progress'
      ).length
    : 0

  return { totalPlanned, totalLive, totalInProgress }
}

export default function Home() {
  const { productRequests } = useDataContext()

  const { totalPlanned, totalLive, totalInProgress } =
    roadmapCardData(productRequests)
  const filters: string[] = ['all', 'ui', 'ux', 'enhancement', 'feature', 'bug']

  const [activeProductRequests, setActiveProductRequests] =
    useState(productRequests)
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [activeFilter, setActiveFilter] = useState('all')

  function toggleMenu() {
    setIsMenuOpen(!isMenuOpen)
  }

  function changeActiveFilter(active: string) {
    setActiveFilter(active)
  }

  useEffect(() => {
    let updatedRequests: ProductRequest[]
    activeFilter === 'all'
      ? (updatedRequests = productRequests)
      : (updatedRequests = productRequests.filter(
          (req) => req.category.toLowerCase() == activeFilter
        ))
    setActiveProductRequests(updatedRequests)
  }, [activeFilter, productRequests])

  return (
    <>
      <div className={styles['cont']}>
        {isMenuOpen && (
          <MobileNav open={isMenuOpen} closeMenu={toggleMenu}>
            <TagCard
              activeFilter={activeFilter}
              changeActiveFilter={changeActiveFilter}
              filters={filters}
            />
            <RoadmapCard
              planned={totalPlanned}
              inprogress={totalInProgress}
              live={totalLive}
            />
          </MobileNav>
        )}
        <LeftCont
          activeFilter={activeFilter}
          filters={filters}
          openMenu={toggleMenu}
          changeActiveFilter={changeActiveFilter}
          planned={totalPlanned}
          inprogress={totalInProgress}
          live={totalLive}
        />
        <RightCont productRequests={activeProductRequests} />
      </div>
    </>
  )
}
