import Link from 'next/link'
import Image from 'next/image'

import SuggestionImage from '@/assets/suggestions/icon-suggestions.svg'
import { sortTypes } from '@/types/sortTypes'

import Select from '@/components/select'

import styles from './page.module.scss'

export default function Banner({
  suggestions,
  filters,
  activeSortFilter,
  setActiveSortFilter,
}: {
  suggestions: number
  filters: string[]
  activeSortFilter: sortTypes
  setActiveSortFilter: (filter: sortTypes) => void
}) {
  // const sortingOptions = [
  //   { label: 'Least Upvotes', value: 'leastUpvotes' },
  //   { label: 'Most Upvotes', value: 'mostUpvotes' },
  //   { label: 'Least Comments', value: 'leastUpvotes' },
  //   { label: 'Most Comments', value: 'mostUpvotes' },
  // ]

  return (
    <>
      <div className={styles['banner']}>
        <Image src={SuggestionImage} alt='Suggestion' />
        <div>{suggestions} Suggestions</div>
        <div>
          <div>Sort By :{/* <span>{activeSortFilter}</span> */}</div>
          <Select
            active={activeSortFilter}
            changeActive={setActiveSortFilter}
            options={filters}
            containerStyles={false}
            containerClassName={styles['select']}
            optionsContainerClassName={styles['select__options']}
            iconColor='white'
          />
        </div>

        <div>
          <Link href={'/feedback/add'}>
            <button className='btn btn--primary btn--wide'>
              + Add Feedback
            </button>
          </Link>
        </div>
      </div>
    </>
  )
}
