'use client'
import { ChangeEvent, useState } from 'react'
import { useRouter } from 'next/navigation'

import styles from './index.module.scss'
import Select from '../select'
import { useDataContext } from '@/context/datacontext'
import { ProductRequest } from '@/types/productRequest'

// TODO: Change Category back to the required categories
// TODO: Add labels to html form elements

interface FeedbackDataProps {
  title: string
  category: string
  status: string
  description: string
}

// FIXME: Fix the types

export default function EditFeedback({ req }: { req: ProductRequest }) {
  const feedback = req

  const router = useRouter()

  const { updateFeedback, removeFeedback } = useDataContext()
  const [title, setTitle] = useState(feedback ? feedback.title : '')
  const [category, setCategory] = useState(feedback ? feedback.category : '')
  const [status, setStatus] = useState(feedback ? feedback.status : '')
  const [description, setDescription] = useState(
    feedback ? feedback.description : ''
  )

  function handleFeedbackUpdate() {
    updateFeedback &&
      updateFeedback({
        id: feedback.id,
        title: title,
        category: category,
        status: status,
        description: description,
      })
    router.back()
  }

  function handleDelete() {
    removeFeedback(feedback.id)
    router.push('/')
  }

  return (
    <>
      <div className={styles['edit-form']}>
        <div className={styles['edit-form__field']}>
          <div className={styles['edit-form__field--title']}>
            Feedback Title
          </div>
          <div className={styles['edit-form__field--subtitle']}>
            Add a short, descriptive headline
          </div>
          <input
            type='text'
            value={title}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setTitle(e.target.value)
            }
          />
        </div>
        <div className={styles['edit-form__field']}>
          <div className={styles['edit-form__field--title']}>Category</div>
          <div className={styles['edit-form__field--subtitle']}>
            Choose a category for your feedback
          </div>
          <Select
            active={category}
            options={['ui', 'ux', 'enhancement', 'feature', 'bug']}
            changeActive={(category: string) => setCategory(category)}
          />
        </div>
        <div className={styles['edit-form__field']}>
          <div className={styles['edit-form__field--title']}>Update Status</div>
          <div className={styles['edit-form__field--subtitle']}>
            Choose a category for your feedback
          </div>
          <Select
            active={status}
            options={['suggestion', 'planned', 'in-progress', 'live']}
            changeActive={(status: string) => setStatus(status)}
          />
        </div>
        <div>
          <div className={styles['edit-form__field--title']}>
            Feedback Detail
          </div>
          <div className={styles['edit-form__field--subtitle']}>
            Include any specific comments on what should be improved, added,
            etc.
          </div>
          <input
            type='text'
            value={description}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setDescription(e.target.value)
            }
          />
        </div>
        <div className={styles['edit-form__buttons']}>
          <button className='btn btn--danger btn--wide' onClick={handleDelete}>
            Delete
          </button>
          <button
            className='btn btn--secondary btn--wide'
            onClick={() => router.back()}
          >
            Cancel
          </button>
          <button
            className='btn btn--primary btn--wide'
            onClick={handleFeedbackUpdate}
          >
            Save Changes
          </button>
        </div>
      </div>
    </>
  )
}
