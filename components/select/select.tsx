'use client'
import { useState, useRef } from 'react'

import useClickOutside from '@/hooks/useClickOutside'

import styles from './select.module.scss'

function DownSvg({ color }: { color?: string }) {
  return (
    <svg width='10' height='7' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M1 1l4 4 4-4'
        stroke={color ? color : '#4661E6'}
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='2'
        fill='none'
        fillRule='evenodd'
      />
    </svg>
  )
}

function CheckedSvg() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='13' height='11'>
      <path
        fill='none'
        stroke='#AD1FEA'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='2'
        d='M1 5.233L4.522 9 12 1'
      />
    </svg>
  )
}

export function Select({
  active,
  options,
  changeActive,
  containerStyles = true,
  containerClassName,
  optionsContainerClassName,
  iconColor,
}: {
  active: string
  options: string[]
  changeActive: Function
  containerStyles?: boolean
  containerClassName?: string
  optionsContainerClassName?: string
  iconColor?: string
}) {
  const [open, setOpen] = useState(false)

  function toggleOpen() {
    setOpen(!open)
  }
  const ref = useRef(null)

  useClickOutside(ref, () => {
    open && toggleOpen()
  })

  function setActive(option: string) {
    changeActive(option)
    toggleOpen()
  }

  return (
    <>
      <div ref={ref}>
        <div
          className={`${styles['select']} ${
            containerStyles ? styles['select-cont'] : ''
          } ${containerClassName}`}
          onClick={toggleOpen}
        >
          <div>{active}</div>
          <DownSvg color={iconColor} />
          {open && (
            <div
              className={`${styles['options']} ${
                optionsContainerClassName && optionsContainerClassName
              }`}
            >
              {options.map((option, index) => {
                return (
                  <div
                    className={styles.option}
                    key={index}
                    onClick={() => setActive(option)}
                  >
                    {option}
                    {option == active && <CheckedSvg />}
                  </div>
                )
              })}
            </div>
          )}
        </div>
      </div>
    </>
  )
}
