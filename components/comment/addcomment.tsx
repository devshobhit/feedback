'use client'
import { ChangeEvent, useState } from 'react'

import { useDataContext } from '@/context/datacontext'
import { user } from '@/types/comments'

import styles from './index.module.scss'

function AddComment({ id, user }: { id: number; user: user }) {
  const [comment, setComment] = useState('')
  const [limit, setLimit] = useState(255)
  const { addComment } = useDataContext()

  function handleChange(e: ChangeEvent<HTMLInputElement>) {
    if (e.target.value.length <= 255) {
      setComment(e.target.value)
      setLimit(255 - e.target.value.length)
    }
  }

  function handleClick() {
    addComment({
      id: id,
      comment: {
        id: Math.random(), //BUG
        content: comment,
        user: user,
      },
    })
    setComment('')
    setLimit(255)
  }

  return (
    <div className={styles['addcomment']}>
      <div className={styles['addcomment__header']}>Add Comment</div>
      {/* 
      FIXME: Change it to TextArea
      */}
      <input
        className={styles['addcomment__input']}
        value={comment}
        onChange={handleChange}
        placeholder='Type your comment here'
      />
      <div className={styles['addcomment__footer']}>
        <div className={styles['addcomment__limit']}>{limit} char left</div>
        <button className='btn btn--wide btn--primary' onClick={handleClick}>
          Post Comment{' '}
        </button>
      </div>
    </div>
  )
}

export default AddComment
