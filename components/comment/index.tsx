'use client'
import { useState, ChangeEvent } from 'react'
import { useDataContext } from '@/context/datacontext'
import { comment } from '@/types/comments'

import Image from 'next/image'
import { usePathname } from 'next/navigation'

import styles from './index.module.scss'

// FIXME: When Reply content is empty disable the button to reply

function ReplyInput({
  commentid,
  replyingTo,
  toggleActive,
}: {
  commentid: number
  replyingTo: string
  toggleActive: () => void
}) {
  const [replytext, setReplyText] = useState('')
  const { addReply, currentUser } = useDataContext()

  const pathname = usePathname()
  const splittedpath = pathname.split('/')
  const activeproductreq = splittedpath[splittedpath.length - 1]

  function handleChange(e: ChangeEvent<HTMLInputElement>) {
    setReplyText(e.target.value)
  }

  function handleClick() {
    replytext.length > 0 &&
      addReply({
        id: Number(activeproductreq),
        commentid: commentid,
        reply: {
          replyingTo: replyingTo,
          content: replytext,
          user: currentUser,
        },
      })
    toggleActive()
  }

  return (
    <>
      <div className={styles['reply-input-cont']}>
        <input
          placeholder='Add a reply'
          value={replytext}
          onChange={handleChange}
        />
        <button className='btn btn--primary' onClick={handleClick}>
          Reply
        </button>
      </div>
    </>
  )
}

// FIXME: Look for better way to deal with ts stuff
// FIXME: Make Reply a button
// FIXME: remove user prop and use type instead for ex. comment type and reply type
function Comment({
  id, // used to keep track the comment id, replies have same id as the parent comment
  comment,
  user,
}: {
  id: number
  comment: comment
  user?: string
}) {
  const [active, setActive] = useState(false)

  function toggleActive() {
    setActive(!active)
  }

  return (
    <>
      <div className={styles['comment-wrapper']}>
        <div className={styles['comment']}>
          {/* <div className={styles['comment__user--img']}> */}
          <Image
            src={comment.user.image}
            width='50'
            height='50'
            className={styles['comment__user-img']}
            alt={comment.user.name}
          ></Image>
          {/* </div> */}

          <div className={styles['comment__content']}>
            <div className={styles['comment__content__header']}>
              <div>{comment.user.name}</div>
              <div>
                <div>@{comment.user.username}</div>
                <div onClick={toggleActive}>Reply</div>
              </div>
            </div>
            <div className={styles['comment__content__body']}>
              {user && (
                <span className={styles['comment__content__body__mention']}>
                  @ {user}{' '}
                </span>
              )}
              {comment.content}
            </div>
          </div>
        </div>

        <div>
          {active && (
            <ReplyInput
              replyingTo={comment.user.username}
              commentid={comment.id}
              toggleActive={toggleActive}
            />
          )}
        </div>
      </div>
    </>
  )
}

export default Comment
