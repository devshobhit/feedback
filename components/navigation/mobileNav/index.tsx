import { ReactNode } from 'react'
import { createPortal } from 'react-dom'
import Image from 'next/image'

import CloseIcon from '@/assets/shared/mobile/icon-close.svg'

import styles from './index.module.scss'

/* TODO: Explore the idea of using portal */
/* TODO: Explore whether the created node is removed from the dom after the element is removed */

function Nav({
  children,
  closeMenu,
}: {
  children: ReactNode
  closeMenu: () => void
}) {
  return (
    <div className={styles['mobile-nav__cont']}>
      <div className={styles['mobile-nav__cont__content']}>
        <Image
          src={CloseIcon}
          className={styles['mobile-nav__cont__icon']}
          alt='X'
          onClick={closeMenu}
        />
        {children}
      </div>
    </div>
  )
}

export default function MobileNav({
  children,
  open,
  closeMenu,
}: {
  children: ReactNode
  open: boolean
  closeMenu: () => void
}) {
  return (
    <>
      {open &&
        createPortal(
          <Nav closeMenu={closeMenu}>{children}</Nav>,
          document.body
        )}
    </>
  )
}
