'use client'
import { useRouter } from 'next/navigation'
import Image from 'next/image'

import ArrowLeft from '@/assets/shared/icon-arrow-left.svg'
import styles from './backbutton.module.scss'

function BackButton() {
  const router = useRouter()

  return (
    <>
      <div className={styles['backbtn']} onClick={() => router.back()}>
        <Image src={ArrowLeft} alt='<' />
        Go Back
      </div>
    </>
  )
}

export default BackButton
