'use client'
import { useDataContext } from '@/context/datacontext'
import useMediaQuery from '@/hooks/usemediaquery'

import Tab from './layout/tab'
import Grid from './layout/grid'

function DataGrid() {
  const { productRequests } = useDataContext()
  const roadmap = {
    planned: productRequests.filter((request) => request.status == 'planned'),
    inProgress: productRequests.filter(
      (request) => request.status == 'in-progress'
    ),
    live: productRequests.filter((request) => request.status == 'live'),
  }

  const isSmallScreen = useMediaQuery('(max-width: 960px)')
  return (
    <>
      {isSmallScreen ? <Tab roadmap={roadmap} /> : <Grid roadmap={roadmap} />}
    </>
  )
}

export default DataGrid
