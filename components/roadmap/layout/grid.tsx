import { roadmapData } from '@/types/roadmap'

import ScrollableContainer from '@/components/containers/scrollable'
import { RoadmapDetailsCard } from '@/components/card'

import styles from './index.module.scss'

function Grid({ roadmap }: { roadmap: roadmapData }) {
  return (
    <div className={styles['grid']}>
      <div className={styles['grid__items']}>
        <div className={styles['grid__items__header']}>
          <div>Planned {`(${roadmap.planned.length})`}</div>
          <div className={styles['grid__items__header__desc']}>
            Ideas prioritized for research
          </div>
        </div>
        <ScrollableContainer>
          {roadmap.planned.map((item) => (
            <div
              className={styles['grid__items--planned']}
              key={'Planned ' + item.id}
            >
              <RoadmapDetailsCard card={item} />
            </div>
          ))}
        </ScrollableContainer>
      </div>

      <div className={styles['grid__items']}>
        <div className={styles['grid__items__header']}>
          <div>In-Progress {`(${roadmap.inProgress.length})`}</div>
          <div className={styles['grid__items__header__desc']}>
            Currently being developed
          </div>
        </div>
        <ScrollableContainer>
          {roadmap.inProgress.map((item) => (
            <div
              className={styles['grid__items--inprogress']}
              key={'inProgress ' + item.id}
            >
              <RoadmapDetailsCard card={item} />
            </div>
          ))}
        </ScrollableContainer>
      </div>

      <div className={styles['grid__items']}>
        <div className={styles['grid__items__header']}>
          <div>Live {`(${roadmap.live.length})`} </div>
          <div className={styles['grid__items__header__desc']}>
            Released Features
          </div>
        </div>
        <ScrollableContainer>
          {roadmap.live.map((item) => (
            <div
              className={styles['grid__items--live']}
              key={'live ' + item.id}
            >
              <RoadmapDetailsCard card={item} />
            </div>
          ))}{' '}
        </ScrollableContainer>
      </div>
    </div>
  )
}

export default Grid
