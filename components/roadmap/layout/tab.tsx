'use client'
import { useState } from 'react'

import { roadmapData } from '@/types/roadmap'

import { RoadmapDetailsCard } from '@/components/card'

import styles from './index.module.scss'

function TabHeader({
  roadmap,
  activeTab,
  changeTab,
}: {
  roadmap: roadmapData
  activeTab: number
  changeTab: Function
}) {
  return (
    <div className={styles['tab-header__cont']}>
      <div
        className={
          styles[`tab-header__item${activeTab === 0 ? '--active' : ''}`]
        }
        onClick={() => changeTab(0)}
      >
        Planned {`(${roadmap.planned.length})`}
      </div>
      <div
        className={
          styles[`tab-header__item${activeTab === 1 ? '--active' : ''}`]
        }
        onClick={() => changeTab(1)}
      >
        In-Progress {`(${roadmap.inProgress.length})`}
      </div>
      <div
        className={
          styles[`tab-header__item${activeTab === 2 ? '--active' : ''}`]
        }
        onClick={() => changeTab(2)}
      >
        Live {`(${roadmap.live.length})`}
      </div>
    </div>
  )
}

function TabContent({
  activeTab,
  roadmap,
}: {
  activeTab: number
  roadmap: roadmapData
}) {
  return (
    <>
      {activeTab === 0
        ? roadmap.planned.map((item) => (
            <div
              // className={styles['datagrid__items--planned']}
              key={'Planned ' + item.id}
            >
              <RoadmapDetailsCard card={item} />
            </div>
          ))
        : activeTab === 1
        ? roadmap.inProgress.map((item) => (
            <div
              // className={styles['datagrid__items--planned']}
              key={'InProgress ' + item.id}
            >
              <RoadmapDetailsCard card={item} />
            </div>
          ))
        : roadmap.live.map((item) => (
            <div
              // className={styles['datagrid__items--planned']}
              key={'Live ' + item.id}
            >
              <RoadmapDetailsCard card={item} />
            </div>
          ))}
    </>
  )
}

function Tab({ roadmap }: { roadmap: roadmapData }) {
  const roadmapStages = [
    {
      name: `Planned (${roadmap.planned.length})`,
      details: 'Ideas prioritized for research',
    },
    {
      name: `In-Progress (${roadmap.inProgress.length})`,
      details: 'Currently being developed',
    },
    { name: `Live (${roadmap.live.length})`, details: 'Released Features' },
  ]

  const [active, setActive] = useState(0)

  function changeTab(idx: number) {
    setActive(idx)
  }

  return (
    <>
      <div className={styles['tab__cont']}>
        <TabHeader roadmap={roadmap} activeTab={active} changeTab={changeTab} />
      </div>
      <div className={styles['tab__cont__title']}>
        {roadmapStages[active].name}
      </div>
      <div className={styles['tab__cont__description']}>
        {roadmapStages[active].details}
      </div>
      <TabContent activeTab={active} roadmap={roadmap} />
    </>
  )
}

export default Tab
