import Link from 'next/link'
import Image from 'next/image'

import { ProductRequest } from '@/types/productRequest'

import { Upvote } from '@/components/upvote/upvote'

import CommentsIcon from '@/assets/shared/icon-comments.svg'

import styles from './index.module.scss'

export default function RoadmapDetailsCard({ card }: { card: ProductRequest }) {
  return (
    <div className={styles['roadmap-details-card']}>
      <div className={styles[`roadmap-details-card--${card.status}`]}>
        <div
          className={`${styles['roadmap-details-card__status']} ${
            styles[`roadmap-details-card__status--${card.status}`]
          }`}
        >
          {card.status}
        </div>
        <div className={styles['roadmap-details-card__title']}>
          <Link
            className={styles['roadmap-details-card__link']}
            href={`/feedback/details/${card.id}`}
          >
            {card.title}
          </Link>
        </div>
        <div className={styles['roadmap-details-card__description']}>
          {card.description}
        </div>
        <div>
          <div className='tag'>{card.category}</div>
        </div>
        <div className={styles['roadmap-details-card__footer']}>
          <Upvote
            requestId={card.id}
            totalUpvotes={card.upvotes}
            upvoted={card.upvoted}
            horizontalLayout={true}
          />
          <div className={styles['roadmap-details-card__comments']}>
            <Image src={CommentsIcon} alt='Comments' />
            {card.comments.length}
          </div>
        </div>
      </div>
    </div>
  )
}
