import Card from '../../card'

import styles from './index.module.scss'

export default function TagCard({
  filters,
  activeFilter,
  changeActiveFilter,
}: {
  filters: string[]
  activeFilter: string
  changeActiveFilter: (arg: string) => void
}) {
  return (
    <>
      <Card>
        <div className={styles['tag-card']}>
          {filters.map((filter, index) => (
            <div
              key={'Filters ' + index}
              className={`tag ${
                activeFilter.toLowerCase() === filter.toLowerCase()
                  ? 'tag--active'
                  : ''
              }`}
              onClick={() => changeActiveFilter(filter)}
            >
              {filter}
            </div>
          ))}
        </div>
      </Card>
    </>
  )
}
