import Link from 'next/link'

import Card from '../../card'

import styles from './index.module.scss'

export default function RoadmapCard({
  planned,
  inprogress,
  live,
}: {
  planned: number
  inprogress: number
  live: number
}) {
  return (
    <>
      <Card>
        <div className={styles['roadmap-card']}>
          <div className={styles['roadmap-card__header']}>
            <div className={styles['roadmap-card__title']}>Roadmap</div>
            <Link href={'/roadmap'} className={styles['roadmap-card__link']}>
              View
            </Link>
          </div>

          <div className={styles['roadmap-card__content--planned']}>
            <span>Planned</span>
            <span>{planned}</span>
          </div>
          <div className={styles['roadmap-card__content--inprogress']}>
            <span>in-progress</span>
            <span>{inprogress}</span>
          </div>
          <div className={styles['roadmap-card__content--live']}>
            <span> live</span>
            <span>{live}</span>
          </div>
        </div>
      </Card>
    </>
  )
}
