import Image from 'next/image'

import MenuIcon from '@/assets/shared/mobile/icon-hamburger.svg'

import styles from './index.module.scss'

export default function BrandCard({ openMenu }: { openMenu: () => void }) {
  return (
    <>
      <div className={styles['brand-card']}>
        <div>
          <div className={styles['brand-card__heading']}>Frontend Mentor</div>
          <div className={styles['brand-card__subheading']}>Feedback Board</div>
        </div>
        <Image
          src={MenuIcon}
          alt='Menu'
          className={styles['brand-card__menu']}
          onClick={openMenu}
        />
      </div>
    </>
  )
}
