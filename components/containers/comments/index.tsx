import { Fragment } from 'react'
import { usePathname } from 'next/navigation'

import { comment } from '@/types/comments'

import Comment from '@/components/comment'
import ScrollableContainer from '../scrollable'

import styles from './index.module.scss'

// FIXME: Make a separate contaner for reply -> comment id issue

//TODO: use pathname thing in comment component
export function Comments({ id, data }: { id: number; data: comment }) {
  const pathname = usePathname()
  const splitted = pathname.split('/')

  return (
    <div className='comment_cont' key={'Comment' + data.id + Math.random()}>
      <Comment id={id} comment={data} />
      <div className={styles['reply_cont']}>
        {data.replies &&
          data.replies.map((reply) => {
            const modifiedReply = {
              id: data.id,
              ...reply,
              content: reply.content,
            }
            return (
              <Comment
                key={'Comment- ' + id}
                id={id}
                comment={modifiedReply}
                user={reply.replyingTo}
              />
            )
          })}
      </div>
    </div>
  )
}

export default function CommentsCont({
  id,
  comments,
}: {
  id: number
  comments: comment[]
}) {
  return (
    <div className={styles['cont']}>
      <div className={styles['cont__header']}>{comments.length} Comments</div>

      {/* <ScrollableContainer height={comments.length > 0 ? '200px' : '20px'}> */}
      {comments.map((comment) => (
        // <Comment comment={comment} />
        <Fragment key={'Comment' + comment.id}>
          <Comments id={id} data={comment} />
        </Fragment>
      ))}
      {/* </ScrollableContainer> */}
    </div>
  )
}
