'use client'
import Image from 'next/image'
import Link from 'next/link'

import { useDataContext } from '@/context/datacontext'
import { ProductRequest } from '@/types/productRequest'

import ArrowUp from '@/assets/shared/icon-arrow-up.svg'
import CommentIcon from '@/assets/shared/icon-comments.svg'

import styles from './index.module.scss'
import { Upvote } from '@/components/upvote/upvote'

/* 
TODO: Making the user mentions Clickable
TODO: Implement upvotes on a request
*/

export default function ProductRequestCont({ req }: { req: ProductRequest }) {
  // const { upvoteFeedback } = useDataContext()
  // const upvoted = req.upvoted
  const activeComments = req.comments.length > 0 ? true : false

  // FIXME: Use SVGs and make the container and change color of active upvote

  return (
    <>
      {/* Left */}
      <div className={styles['cont']}>
        <div className={styles['cont__left']}>
          <Upvote
            requestId={req.id}
            upvoted={req.upvoted}
            totalUpvotes={req.upvotes}
          />
          {/* <button
            className={`${styles['cont__upvotes']} ${
              upvoted && styles['cont__upvotes--active']
            }`}
            onClick={() => upvoteFeedback(req.id)}
          >
            <Image src={ArrowUp} alt='^' />
            {req.upvotes}
          </button> */}
        </div>

        {/* Right */}
        <div className={styles['cont__right']}>
          <div>
            <Link
              href={`/feedback/details/${req.id}`}
              className={styles['cont__link']}
            >
              <div className={styles['cont__title']}>{req.title}</div>
            </Link>
            <div className={styles['cont__description']}>{req.description}</div>

            <div className='tag'>{req.category}</div>
          </div>
          <div
            className={`${styles['cont__comments']} ${
              activeComments && styles['cont__comments--active']
            }`}
          >
            <Image src={CommentIcon} alt='comments' />
            <span>{req.comments.length}</span>
          </div>
        </div>
      </div>
    </>
  )
}
