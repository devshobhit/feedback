import Image from 'next/image'
import Link from 'next/link'

import EmptyImage from '@/assets/suggestions/illustration-empty.svg'

import styles from './index.module.scss'

// className ={styles['empty']}

export default function EmptyCont() {
  return (
    <>
      <div className={styles['empty-cont']}>
        <Image
          src={EmptyImage}
          alt='Add Feedback'
          className={styles['empty-cont__img']}
        />
        <h1 className={styles['empty-cont__heading']}>
          There is no feedback yet.
        </h1>
        <h2 className={styles['empty-cont__subheading']}>
          Got a suggestion? Found a bug that needs to be squashed? We love
          hearing about new ideas to improve our app.
        </h2>
        <Link href={'/feedback/add'}>
          <button className='btn btn--primary'>+ Add Feedback</button>
        </Link>
      </div>
    </>
  )
}
