import { ReactNode } from 'react'
import styles from './index.module.scss'

export default function ScrollableCont({
  children,
}: // height,
{
  children: ReactNode
  height?: string
}) {
  return (
    <>
      <div
        className={styles['scrollable-cont']}
        // style={{
        //   height: height ? height : '550px',
        // }}
      >
        {children}
      </div>
    </>
  )
}
