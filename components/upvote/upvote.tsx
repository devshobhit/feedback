import Image from 'next/image'

import { useDataContext } from '@/context/datacontext'

import ArrowUp from '@/assets/shared/icon-arrow-up.svg'

import styles from './upvote.module.scss'

export function Upvote({
  requestId,
  totalUpvotes,
  upvoted,
  horizontalLayout,
}: {
  requestId: number
  totalUpvotes: number
  upvoted: boolean
  horizontalLayout?: boolean
}) {
  const { upvoteFeedback } = useDataContext()

  return (
    <button
      className={`${styles['upvote']} ${upvoted && styles['upvote--active']} ${
        horizontalLayout && styles['layout-horizontal']
      }`}
      onClick={() => upvoteFeedback(requestId)}
    >
      <Image src={ArrowUp} alt='^' />
      {totalUpvotes}
    </button>
  )
}
