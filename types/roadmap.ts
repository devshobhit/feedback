import { ProductRequest } from './productRequest'

export type roadmapData = {
  planned: ProductRequest[]
  inProgress: ProductRequest[]
  live: ProductRequest[]
}
