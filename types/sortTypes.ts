export type sortTypes =
  | 'Most Upvotes'
  | 'Most Comments'
  | 'Least Upvotes'
  | 'Least Comments'
