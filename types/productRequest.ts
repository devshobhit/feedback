import { comment } from './comments'

export type ProductRequest = {
  id: number
  title: string
  category: string
  upvotes: number
  upvoted: boolean
  status: string
  description: string
  comments: comment[]
}
