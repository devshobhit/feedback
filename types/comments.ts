import { StaticImageData } from 'next/image'

export type user = {
  image: StaticImageData
  name: string
  username: string
}
export type reply = {
  content: string
  replyingTo: string
  user: user
}

export type comment = {
  id: number
  content: string
  user: user
  replies?: reply[]
}
