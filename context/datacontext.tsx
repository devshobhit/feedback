'use client'
import { createContext, useContext, useState } from 'react'
import { ReactNode } from 'react'
import { StaticImageData } from 'next/image'

import { ProductRequest } from '@/types/productRequest'
import { reply, user, comment } from '@/types/comments'

import { data } from '@/utils/data'

interface updateFeedback {
  id: number
  title: string
  // category: 'feature' | 'ui' | 'ux' | 'enhancement' | 'bug'
  // status: 'suggestion' | 'planned' | 'in-progress' | 'live'
  category: string
  status: string
  description: string
}

interface AddFeedback {
  title: string
  // category: 'feature' | 'ui' | 'ux' | 'enhancement' | 'bug'
  category: string
  description: string
}

interface DataContextType {
  currentUser: { name: string; username: string; image: StaticImageData }
  productRequests: ProductRequest[]
  updateFeedback: (payload: updateFeedback) => void
  addFeedback: (payload: AddFeedback) => void
  upvoteFeedback: (id: number) => void
  addReply: (payload: { id: number; commentid: number; reply: reply }) => void
  addComment: (payload: { id: number; comment: comment }) => void
  removeFeedback: (id: number) => void
}

// FIXME: This context would need refactor (Shift to redux State Management)
// TODO: Identify the issue the value passed via payload is treated as string irrespective of the number type. Also no error in ts

const Context = createContext<DataContextType | null>(null)

export function DataProvider({ children }: { children: ReactNode }) {
  const [productRequests, setProductRequests] = useState(data.productRequests) // Temporary workaround

  // FIXME: Add backend request to db
  function addFeedback(payload: {
    title: string
    // category: 'feature' | 'ui' | 'ux' | 'enhancement' | 'bug'
    category: string
    description: string
  }) {
    // console.log('Adding the feedback')
    const feedback: ProductRequest = {
      id: productRequests.length + 1,
      title: payload.title,
      category: payload.category,
      upvotes: 0,
      upvoted: false,
      status: 'suggestion',
      description: payload.description,
      comments: [],
    }
    setProductRequests([...productRequests, feedback])
    // console.log('Adding Feedback', productRequests)
  }

  function updateFeedback(payload: {
    id: number
    title: string
    // category: 'feature' | 'ui' | 'ux' | 'enhancement' | 'bug'
    // status: 'suggestion' | 'planned' | 'in-progress' | 'live'
    category: string
    status: string
    description: string
  }) {
    const productrequests = productRequests.map((productreq) => {
      if (productreq.id === payload.id) {
        productreq.title = payload.title
        productreq.category = payload.category
        productreq.status = payload.status
        productreq.description = payload.description
      }
      return productreq
    })

    setProductRequests(productrequests)
  }

  function removeFeedback(id: number) {
    const productrequests = productRequests.filter(
      (productreq) => productreq.id != id
    )

    setProductRequests(productrequests)
  }

  function upvoteFeedback(id: number) {
    const productrequests = productRequests.map((productreq) => {
      if (productreq.id === id) {
        // productreq.upvoted = !productreq.upvoted
        // productreq.upvoted ? productreq.upvotes++ : productreq.upvotes--
        if (!productreq.upvoted) {
          productreq.upvoted = true
          productreq.upvotes++
        } else {
          productreq.upvoted = false
          productreq.upvotes--
        }
      }
      return productreq
    })

    setProductRequests(productrequests)
  }

  function addReply(payload: { id: number; commentid: number; reply: reply }) {
    const productrequests = productRequests.map((productreq) => {
      if (productreq.id === Number(payload.id)) {
        console.log(payload.commentid)
        productreq.comments = productreq.comments.map((comment) => {
          if (comment.id === Number(payload.commentid)) {
            if (!comment.replies) {
              comment = { ...comment, replies: [payload.reply] }
            } else {
              comment.replies = [...comment.replies, payload.reply]
            }
          }
          return comment
        })
      }
      return productreq
    })

    setProductRequests(productrequests)
  }

  function addComment(payload: { id: number; comment: comment }) {
    const productrequests = productRequests.map((productreq) => {
      if (productreq.id === Number(payload.id)) {
        productreq.comments = [...productreq.comments, payload.comment]
      }
      return productreq
    })

    setProductRequests(productrequests)
  }

  return (
    <Context.Provider
      value={{
        currentUser: data.currentUser,
        productRequests: productRequests,
        addFeedback: addFeedback,
        updateFeedback: updateFeedback,
        upvoteFeedback: upvoteFeedback,
        addReply: addReply,
        addComment: addComment,
        removeFeedback: removeFeedback,
      }}
    >
      {children}
    </Context.Provider>
  )
}

export function useDataContext() {
  // return useContext(Context)
  const context = useContext(Context)
  if (!context) {
    throw new Error('Please use appropriate Data Context')
  }
  return context
}
