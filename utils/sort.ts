import { ProductRequest } from '@/types/productRequest'
import { sortTypes } from '@/types/sortTypes'

function sortByCommentsAsc(productRequest: ProductRequest[]) {
  return productRequest.sort(
    (req1, req2) => req1.comments.length - req2.comments.length
  )
}

function sortByCommentsDesc(productRequest: ProductRequest[]) {
  return productRequest.sort(
    (req1, req2) => req2.comments.length - req1.comments.length
  )
}

function sortByUpvotesAsc(productRequest: ProductRequest[]) {
  return productRequest.sort((req1, req2) => req1.upvotes - req2.upvotes)
}

function sortByUpvotesDesc(productRequest: ProductRequest[]) {
  return productRequest.sort((req1, req2) => req2.upvotes - req1.upvotes)
}

export function sortProductRequests(
  productRequests: ProductRequest[],
  filter: sortTypes
) {
  const sortWithFilter = {
    'Least Comments': sortByCommentsAsc,
    'Most Comments': sortByCommentsDesc,
    'Least Upvotes': sortByUpvotesAsc,
    'Most Upvotes': sortByUpvotesDesc,
  }

  const clonedReq = [...productRequests]

  sortWithFilter[filter](clonedReq)

  return clonedReq
}
