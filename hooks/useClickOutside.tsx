import { MutableRefObject, ReactNode, RefObject, useEffect } from 'react'

function useClickOutside(ref: RefObject<HTMLElement>, callback: Function) {
  useEffect(() => {
    function handleClickOutside(event: MouseEvent) {
      if (ref.current && !ref.current.contains(event.target as Node)) {
        callback()
      }
    }

    // Add event listener
    document.addEventListener('mousedown', handleClickOutside)

    // Cleanup function
    return () => {
      // Remove event listener
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [ref, callback])
}

export default useClickOutside
